const textarea = document.querySelector("textarea");
const katexDiv = document.querySelector("div#katex");
const katexOutDiv = document.querySelector("div#katex-out");

function displayMath(data) {
	let out = "";

	for (let i = 0; i < data.solutions.length; i++) {
		let sol = data.solutions[i];

		if (!sol.step_input) {
			sol.step_input = "";
		}
		if (!sol.entire_result) {
			sol.entire_result = `\\textit{ ${sol.solvingClass}}`;
		} else {
			sol.entire_result = `${sol.entire_result} \\textit{ (${sol.solvingClass})}`;
		}

		let letter = String.fromCharCode("A".charCodeAt() + i);
		let solution = `\\[\\textbf{Solution ${letter}}\\]\\[${sol.step_input}\\\\${sol.entire_result}\\]`;

		let table = "<table>";
		for (let step of sol.steps) {
			let info = "";
			if (step.title) {
				info = `\\[${step.title.text.createdText.replace(/ /g, "\\ ").replace(/<br\/>/g, "\\\\\\\\")}\\]`;
			} else if (step.steps) {
				info = "<table class=\"border\">";
				for (let step2 of step.steps) {
					info += `<tr><td class="info"></td><td>\\(${step2.entire_result}\\)</td></tr>`;
					if (step2.steps) {
						for (let step3 of step2.steps) {
							info += `<tr><td class="info">\\[${step3.title.text.createdText}\\]</td><td>\\(${step3.entire_result}\\)</td></tr>`;
						}
					}
				}
				info += "</table>";
			}
			if (!step.step_input) {
				step.step_input = "\\ ";
			}
			if (!step.entire_result) {
				step.entire_result = "\\ ";
			}
			table += `<tr><td class="info">${info}</td><td>\\(${step.step_input}\\)</td><td>\\(${step.entire_result}\\)</td></tr>`;
		}
		table += "</table>"
		out += `${solution}<br>${table}`

		if (i+1 < data.solutions.length) {
			out += "<br><hr><br>";
		}
	}

	katexOutDiv.innerHTML = out;
	renderMathInElement(katexOutDiv);
}

textarea.addEventListener("input", (e) => {
	katexDiv.innerHTML = `\\[${e.target.value}\\]`;
	renderMathInElement(katexDiv);
});

textarea.addEventListener("keydown", async (e) => {
	if (e.ctrlKey && e.key === "Enter") {
		const data = await window.electronAPI.solve(textarea.value);
		displayMath(data);
	}
});
