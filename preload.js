const { contextBridge, ipcRenderer } = require("electron");

contextBridge.exposeInMainWorld("electronAPI", {
	solve: (expression) => ipcRenderer.invoke("solve", expression)
});
