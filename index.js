const { app, BrowserWindow, ipcMain } = require("electron");
const path = require("path");
const got = require("got");
const { CookieJar } = require("tough-cookie");

let token;

const createWindow = async () => {
	await getCookie();

	const mainWindow = new BrowserWindow({
		width: 800,
		height: 600,
		webPreferences: {
			preload: path.join(__dirname, "preload.js")
		}
	})

	ipcMain.handle("solve", async (_event, expression) => {
		return await dataGetter(expression);
	});

	mainWindow.loadFile("index.html");
}

app.whenReady().then(() => {
	createWindow();

	app.on("activate", () => {
		if (BrowserWindow.getAllWindows().length === 0) { 
			createWindow();
		}
	})
})

app.on("window-all-closed", () => {
	if (process.platform !== "darwin") { 
		app.quit()
	}
})

const getCookie = async () => {
	const cookieJar = new CookieJar();

	await got.get("https://www.symbolab.com/solver/step-by-step/2x-9=23?or=input", {
		headers: {
			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0",
		},
		cookieJar,
	});

	token = (await cookieJar.getCookies("https://www.symbolab.com")).find(x => x.key === "sy2.pub.token").value;
}

const dataGetter = async (expression) => {
	const url = new URL("https://www.symbolab.com/pub_api/steps?subscribed=true&origin=input&language=en&referer=&plotRequest=PlotOptional&page=step-by-step");
	url.searchParams.append("query", expression)

	const response = await got.get(url, {
		headers: {
			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0",
			"Authorization": `Bearer ${token}`,
			"X-Requested-With": "XMLHttpRequest",
		},
	}).catch(async () => {
		console.error("Request failed. Retrying.");
		getCookie();
		return (await dataGetter());
	});

	return JSON.parse(response.body);
}
