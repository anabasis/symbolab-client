# symbolab-client
A client for Symbolab made with Electron.

Run `npm start` to start the application and type in your expression in LaTeX to use it.

## License
`symbolab-client` is licensed under the MIT License.
